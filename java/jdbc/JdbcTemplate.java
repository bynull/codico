package jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Jdbc template like spring jdbc template
 * Created by bynull on 16.09.15.
 */
public class JdbcTemplate {
    private static final Logger log = LoggerFactory.getLogger(JdbcTemplate.class);
    private final Connection connection;

    public JdbcTemplate(Connection connection) {
        this.connection = connection;
    }

    public <T> List<T> queryForList(QueryObject<T> queryObject) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = prepateSql(queryObject.query());
            queryObject.setParams(ps);
            rs = ps.executeQuery();

            List<T> results = new ArrayList<T>();
            int rowNum = 0;
            while (rs.next()) {
                results.add(queryObject.map(rs, rowNum));
                rowNum++;
            }

            return results;
        } catch (Exception e) {
            log.warn("Error while executing sql request: " + queryObject.query());
        } finally {
            JdbcUtil.closeResultSet(rs);
            JdbcUtil.closeStatement(ps);
        }
        return new ArrayList<T>();
    }

    private PreparedStatement prepateSql(String sql) throws java.sql.SQLException {
        if (connection == null) {
            throw new java.sql.SQLException("Failed to acquire prepared statement. Connection is null");
        }
        return connection.prepareStatement(sql);
    }

    public interface QueryObject<T> {
        String query();

        void setParams(PreparedStatement ps) throws SQLException;

        T map(ResultSet rs, int rowNum) throws SQLException;
    }
}
